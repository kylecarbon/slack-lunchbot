#!/usr/bin/env python
import json
import os
import random
import requests
from slacker import Slacker


# NOTE: I confusingly use 2 different APIs to post to slack -- one for posting a
# message and another unofficial one for posting the poll.

############ List of lunch options ############
healthful_lunch_options = ["GrabbaGreen", "Tender Greens", "Unico"]
tasty_lunch_options = ["Doner", "Rubicon Deli", "RakiRaki :ramen:", "Lotus Thai :flag-th:",
                     "Pho Fifth Avenue :flag-vn:", "Cap's :pizza:", "Ike's", "Pokirrito = :sushi: + :burrito:",
                     "The Poke Co. :sushi:", "Ototo Sushi Co. :sushi:",
                     "El Cuervo :burrito:", "Crack Shack :chicken:", "Sushiya :sushi:", "Carnitas' Snack Shack",
                     "Chris Ono", "Chic Fil A :chicken:", "Tandoori Hut :flag-in:", "Cotijas Taco Shop :taco:",
                     "Enoteca", "Micho'z Fresh Lebanese Grill", "Pop Pie Co. :pie:", "Tandoori House :flag-in:",
                     "Soi 30th :flag-th:", "Aaharn :flag-th:", "Colimas's :burrito:", "55 Thai Kitchen :flag-th:"]

############ API 1: post message to alert channel ############
# hook_url for slack
hook_url = "https://hooks.slack.com/services/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"
# this url points to a picture of Gordon Ramsay
icon_url = "http://themixradio.co.uk/wp-content/uploads/2017/05/2-gordonramsay.jpg"
user_to_message = "#z_lunch"
lunch_message = "Time to vote -- poll closes at 1000 PT."
slack_data = {'text': lunch_message, 'username': "GordonRamsay", 'channel': user_to_message,
              'icon_url': icon_url}

response = requests.post(hook_url, data=json.dumps(slack_data),
                         headers={'Content-Type': 'application/json'})

# Generate lunch options
number_of_lunch_options = 5
todays_lunch_options = random.sample(tasty_lunch_options, number_of_lunch_options)
regular_poll_str = "\"Tasty lunch options\""

number_of_healthful_options = min(5, len(healthful_lunch_options))
todays_healthful_lunch_options = random.sample(healthful_lunch_options, number_of_healthful_options)
healthful_poll_str = "\"Healthful lunch options\""

for option in todays_lunch_options:
    regular_poll_str += " \"" + option + "\""

for healthful_option in todays_healthful_lunch_options:
    healthful_poll_str += " \"" + healthful_option + "\""

############ API 2: post polls ############
dir_path = os.path.dirname(os.path.realpath(__file__))
file_path = os.path.join(dir_path, "slack_key")
file = open(file_path, "r")
slack_key = file.read()
slack = Slacker(slack_key)
channel = "z_lunch"
channel_id = slack.channels.get_channel_id(channel)
slack.chat.command(
    channel=channel_id,
    command="/poll",
    text=regular_poll_str
)

slack.chat.command(
    channel=channel_id,
    command="/poll",
    text=healthful_poll_str
)

if response.status_code != 200:
    raise ValueError('Request to slack returned an error %s, the response is:\n%s' %
                     (response.status_code, response.text))
