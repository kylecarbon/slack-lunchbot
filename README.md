# Installation
First, install all dependencies:
```
pip3 install -r requirements.txt
```
Next, set up a [Slack Legacy Token](https://api.slack.com/custom-integrations/legacy-tokens).
Stick it in a file in `webhooks/slack_key`. `git` will ignore this file but you need it locally.
